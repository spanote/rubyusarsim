#requires the robot team module
require 'control_lib/level2/main/robot_team.rb'

#requires the robot teammap module
require 'control_lib/level2/support/teammap.rb'


team1= RobotTeam.new("searchteam","10.1.157.59",3000)

#values for factory map
team1.addrobot("USARBot.P2AT","3.5,10,1.5","robot1")
team1.addrobot("USARBot.P2AT","12.61,-2.62,1.0","robot2")
team1.addrobot("USARBot.P2AT","17.36,6.41,1.0","robot3")
team1.addlog("robot1")

sleep(1)

map=Teammap.new(team1)
map.configmap(600,600,0.3,0.3)
map.createmap("robot1")
map.createmap("robot2")
map.createmap("robot3")

team1.robotno.each{|robot|
  robot.colguardmode
}

team1.runidm("robot1","mazemove")
team1.runidm("robot2","mazemove")
team1.runidm("robot3","mazemove")

sleep(60*3.5)