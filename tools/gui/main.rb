#USARSIM ROBOTIC CONTROL PLATFORM MADE FOR USARSIM 2007
# BUILT USING RUBY / TK 
# Requires TK Ver 8.4.X and Ruby 1.8.X

#Note To self : updated the init function and added camera function at class robot

#required for advanced control algorithms
require 'gui_control_lib/level1/support/gui_idm.rb'

#required for the camera
require 'gui_control_lib/imageprocess/conserver.rb'

#require the robotic library
require 'gui_control_lib/gui/g_u_i_interface.rb'

#requires the robot team module
require 'gui_control_lib/level1/main/gui_robot.rb'

#requires the robot robotmap module
require 'gui_control_lib/level1/support/gui_robotmap.rb'

#requires the robot log module
require 'gui_control_lib/level1/support/gui_robotlog.rb'


#The main program starts here 

#Generates A Bot Form
Form1=GUI_Interface.new()
Form1.run



