$mapdatalist=[]
$mapobjlist=[]
$mapdatadir="../../mapdata/"
$count=0

def getdirdata()
  dirlist=Dir.entries($mapdatadir)

  $mapdatalist=[]
  $mapobjlist=[]
  dirlist.each{|filename|
     $mapdatalist<<filename  if (filename=~/occ/i and filename=~/data/i)
     $mapobjlist<<filename  if(filename=~/occ/i  and filename=~/obj/i)
  }


if($mapdatalist.size+$mapobjlist.size<2)
    raise "No readable files found "
end
  
  if($DEBUG==1)
  puts "-----"
  puts $mapdatalist
   puts "-----"
  puts $mapobjlist
  end

  #sleep(5)
end

def loaddata(mapname)
 mapdata=[]
 open(mapname,"r"){|file|
  file.each_with_index{|line,index|
  if (index>0)
    insertarray=line.split(",")
    insertarray.pop#remove final empty array 
    mapdata<<Array.new(insertarray) 
  end
  }
 }
  return mapdata
end

def returnmax(number1,number2)
  if (number1.to_f-0.5).abs>(number2.to_f-0.5).abs
    return number1
  else
    return number2
  end
end

def combinedata(mapname1,mapname2,sizex,sizey)
  ldpath1=$mapdatadir+mapname1
  ldpath2=$mapdatadir+mapname2
 
  map1data=loaddata(ldpath1)
  map2data=loaddata(ldpath2)
  
  newmappath=$mapdatadir+"occ_combined#{$count}_data.dat"
  open(newmappath,"w"){|file|
   file.puts "#{sizex},#{sizey}"
   map1data.each_with_index{|mapline,y|
   inslinearray=""
    mapline.each_with_index{|element,x|
      inslinearray<<(returnmax(map2data[y][x].to_s,element)).to_s
      inslinearray<<","
      }
    file.puts inslinearray
    }  
  }

end

def combineobj(mapname1,mapname2)
    ldpath1=$mapdatadir+mapname1
    ldpath2=$mapdatadir+mapname2
   newmappath=$mapdatadir+"occ_combined#{$count}_obj.dat"

   open(newmappath,"w"){|file|
     
     open(ldpath1,"r"){|file1|
       file1.each{|line|
       file.puts line 
       }
     
      open(ldpath2,"r"){|file2|
       file2.each{|line|
       file.puts line 
       }
      
     }
    }
   }
end


puts "scaning mapdata directory"
getdirdata()
puts "found the following files containing map data"
puts $mapdatalist
puts "found the following files containing occ data"
puts $mapobjlist

raise "missing map data file" if($mapdatalist.size<$mapobjlist.size)
raise "missing map obj file" if($mapdatalist.size>$mapobjlist.size)

puts "finding matches"

pairlist=[]
maplist=[]
$mapdatalist.each_with_index{|mapname,datalistindex|
    ldpath=$mapdatadir+mapname
     open(ldpath,"r")do|file|
     insarray=[]
     templine=file.gets.split(",")
     insarray[0]=templine[0]
     insarray[1]=templine[1]
     if(pairlist.size==0)
       pairlist<<insarray
       maplist<<mapname
     else
       pairlist.each_with_index{|member,index|
       if member==insarray
         puts "found match combining maps"
         combinedata(mapname,maplist[index],member[0],member[1])
         combineobj($mapobjlist[datalistindex],$mapobjlist[index])
         $count=$count+1
         break
       end
       }
    end
 end
}



