#requires the robot team module
require 'control_lib/level2/main/robot_team.rb'

#requires the robot teammap module
require 'control_lib/level2/support/dualmap.rb'
require 'control_lib/level2/support/teammap.rb'


team1= RobotTeam.new("searchteam","10.1.157.59",3000)
#for dm_mapping
#team1.addrobot("USARBot.P2AT","0.748,-7.282,-0.446","robot1")
#team1.addrobot("USARBot.P2AT","0.748,-8.282,-0.446","robot2")

#for testmap
#team1.addrobot("USARBot.P2AT","-6.0,-2.0,1.8","robot1")
#team1.addrobot("USARBot.P2AT","2.0,3.0,1.8","robot2")

#for robocup2
#team1.addrobot("USARBot.P2AT","-66.25,11,-4","robot1")

#for factory
team1.addrobot("USARBot.P2AT","3.5,10,1.5","robot1")

sleep(1)

#startlogging
#team1.addlog("robot1")
#team1.addlog("robot2")
#team1.addlog("robot3")
sleep(1)

#team map
map=Teammap.new(team1)
map.createmap("robot1")
#map.createmap("robot2")

#Dual map
#map=Dualmap.new(team1)
#map.autoupdatemap(true)
#map.autologmap(true)


team1.robotno.each{|robot|
  robot.colguardmode
}


#puts "#{team1.robotno[0].ins}"
#team1.robotno[0].smartfor_acc(0.5,2.0)
#team1.robotno[0].smartright(6.28/4)
#
#puts "#{team1.robotno[0].ins}"

team1.runidm("robot1","mazemove")
#team1.runidm("robot2","smartmove")


#Main Loop
#while(true)
# Thread.list.each{|t| puts "#{t.inspect} : #{t[:name]}"}
#planner.findnextpath("robot1")
#sleep(1.0)
#end

sleep(60*10)