

#require the socket library
require 'socket'

#used for converting image formats
require 'rubygems'
require 'Rmagick'

#fun
include Magick

#A class used for streamming the images from the Imageserver program.
#The Image server program must be executed beforehand
class GetImage
  attr_reader :start
  attr_reader :updated
  attr_reader :status
  #create a new connection to the image_server
  def initialize(_ip,_port,_robot,_camera)
    @ip=_ip
    @port=_port
    @robotno=_robot
    @camerano=_camera
    @imginfo=[]
    @imginfo_b=[]
    @start=0
    @updated=0
    
    #connect to the image server
    @trys=0
    begin
    puts "Attempting to Connect to Image server (#{@trys+1}/3 )"
    @connection=TCPSocket.open(@ip,@port)
    rescue Exception
      @trys+=1
      retry if @trys<3
      puts "connection failed"
      @status=0
      return 0 #cannot connect to IP Server
    end
   puts "Connection established "
   @status=1
    
  end
  
  #stop streaming the pictures
  def batchjpg_stop()
    @start=0
  end
  
  #grap one picture from the imageserver
  def getjpg()
    #clear old data
    @imginfo=[]
    @imginfo_b=[]    
    
    #grab the Image info
    5.times{
     @imginfo<<@connection.recv(1)
    }
    
    #Show the data and convert it to a bytearray
    @imginfo.each_with_index{|member,index|
      member.each_byte {|byte| 
        puts "image type= #{byte}" if index==0
        print "image byte size " if index==1
        print " #{byte}" if index>=1
        @imginfo_b<<byte
      }
    }
    
    #calculate the size of the image 
    @size=@imginfo_b[1].to_i*256*256*256 +@imginfo_b[2]*256*256+@imginfo_b[3]*256+@imginfo_b[4]
    if(@size<1)
      puts "Error calculating size data "
      return 0
    end
    puts "" #just insert a new line
    puts "Total jpeg size  #{@size} bytes"
   
   #grab the image 
   @image=""
   @size.times{
    @image<<@connection.recv(1) 
   }
   puts "Total grabbed jpg Image size is #{@image.size}"
   #write the data to file  
    open("Robot#{@robotno}Camera#{@camerano}.jpg","wb") do |file|
      file.puts @image
    end
     
   sleep(0.1)
   #convert the file to a gif image 
   @fimage=Magick::Image.read("Robot#{@robotno}Camera#{@camerano}.jpg").first
   #fun
   #@fimage=@fimage.source.quantize(256,Magick::GrayColorspace)
   #@fimage=@fimage.edge(1)
   #@fimage.write("Robot#{@robotno}Camera#{@camerano}.gif")

   #ask for the next batch of data
   @connection.puts "OK"
   sleep(0.5)
  end
  
  #start grabbing the pictures from the image server, streams the images from the 
  #server until GetImage.batchjpg_stop is executed
  def batchjpg_proc()
    @start=1
    Thread.new(){
    while @start==1    
      
    sleep(0.2)    
    
    #clear old data
    @imginfo=[]
    @imginfo_b=[]    
    
    #grab the Image info
    5.times{
     @imginfo<<@connection.recv(1)
    }
    
    #Show the data and convert it to a bytearray
    @imginfo.each_with_index{|member,index|
      member.each_byte {|byte| 
        puts "image type= #{byte}" if index==0
        print "image byte size " if index==1
        print " #{byte}" if index>=1
        @imginfo_b<<byte
      }
    }
    
    
    #calculate the size of the image 
    @size=@imginfo_b[1].to_i*256*256*256 +@imginfo_b[2]*256*256+@imginfo_b[3]*256+@imginfo_b[4]
    if(@size<1)
      puts "Error calculating size data "
      @connection.puts "OK"
      next
    end
    puts "" #just insert a new line
    puts "Total jpeg size  #{@size} bytes"
   
   #grab the image 
   @image=""
   @size.times{
    @image<<@connection.recv(1) 
   }
   puts "Total grabbed jpg Image size is #{@image.size}"
   if(@image.size>100000)
     puts "Error calculating Image size  "
      @connection.puts "OK"
     next
   end
        
   #write the data to file
   open("Robot#{@robotno}Camera#{@camerano}.jpg","wb") do |file|
   file.puts @image
   end
   sleep(0.2)

          
   #convert the file to a gif image 
   @fimage=Magick::Image.read("Robot#{@robotno}Camera#{@camerano}.jpg").first
   #@fimage.write("Robot#{@robotno}Camera#{@camerano}.gif")
   #fun
   @fimage=@fimage.edge(1)
   @fimage.write("Robot#{@robotno}Camera#{@camerano}.gif")
   puts "Test"
   #sleep(2.0)
   
   #ask for the next batch of data
   @connection.puts "OK"
   @updated=1
   #sleep(0.2)
    end #while loop
    }
  end
  
   #start grabbing the pictures from the image server, streams the images from the 
  #server until GetImage.batchjpg_stop is executed
  def batchjpg()
    @start=1
    Thread.new(){
    while @start==1    
      
    sleep(0.2)    
    
    #clear old data
    @imginfo=[]
    @imginfo_b=[]    
    
    #grab the Image info
    5.times{
     @imginfo<<@connection.recv(1)
    }
    
    #Show the data and convert it to a bytearray
    @imginfo.each_with_index{|member,index|
      member.each_byte {|byte| 
      #  puts "image type= #{byte}" if index==0
       # print "image byte size " if index==1
      #  print " #{byte}" if index>=1
        @imginfo_b<<byte
      }
    }
    
    
    #calculate the size of the image 
    @size=@imginfo_b[1].to_i*256*256*256 +@imginfo_b[2]*256*256+@imginfo_b[3]*256+@imginfo_b[4]
    if(@size<1)
      puts "Error calculating size data "
      @connection.puts "OK"
      next
    end
   # puts "" #just insert a new line
   # puts "Total jpeg size  #{@size} bytes"
   
   #grab the image 
   @image=""
   @size.times{
    @image<<@connection.recv(1) 
   }
  # puts "Total grabbed jpg Image size is #{@image.size}"
   if(@image.size>100000)
    # puts "Error calculating Image size  "
      @connection.puts "OK"
     next
   end
        
   #write the data to file
   open("Robot#{@robotno}Camera#{@camerano}.jpg","wb") do |file|
   file.puts @image
   end
   sleep(0.2)

          
   #convert the file to a gif image 
   @fimage=Magick::Image.read("Robot#{@robotno}Camera#{@camerano}.jpg").first
   @fimage.write("Robot#{@robotno}Camera#{@camerano}.gif")

   #sleep(2.0)
   
   #ask for the next batch of data
   @connection.puts "OK"
   @updated=1
   #sleep(0.2)
    end #while loop
    }
  end
  
    
end
  
#Used for test run
#ip='127.0.0.1'
##port=5003
#puts "Hello"
#Thread.new do
#puts "Hello"
#end
#imgserver=GetImage.new(ip,port,1,1)
#imgserver.batchjpg
#sleep(10) #wait for thread


