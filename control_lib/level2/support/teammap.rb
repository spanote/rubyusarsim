#Added Mutex
require "Benchmark"
require "thread"

#requires the robot robotmap module
require 'control_lib/level1/support/robotmap.rb'


#need to set
#map.autoupdatemap(true)
#map.autologmap(true)
#at the main program


class Teammap
  attr_reader:mapno
  #start the mapping sequence
  def initialize(robotteam)
   @team=Array.new(robotteam.robotno) #create the robot array from the robot team
   
   #map
   @mapno=[]
   @mapnum=0
  end
   
  def setautologmap(robotname,value) 
    #search the robot name 
    @mapno.each_with_index {|map,index|
      #map is an instance of Robotmap
       checkname=map.robotobj.robotname  #get the map's robot name
    if(robotname==checkname)
      puts "setting autolog for robot #{robotname} to #{value}"
      @mapno[index].autologmap(value)
      break
    end
    if(index==@num)
      puts "couldn't find robot with that name"
    end
    }
  end
  
   def setautoupdatemap(robotname,value) 
    #search the robot name 
    @mapno.each_with_index {|map,index|
      #map is an instance of Robotmap
       checkname=map.robotobj.robotname  #get the map's robot name
    if(robotname==checkname)
      puts "setting autolog for robot #{robotname} to #{value}"
      @mapno[index].autoupdatemap(value)
      break
    end
    if(index==@num)
      puts "couldn't find robot with that name"
    end
    }
  end
  
  def configmap(newmapwidth,newmapheight,newcellwidth,newcellheight)
    @nextmapwidth=newmapwidth
    @nextmapheight=newmapheight
    @nextcellwidth=newcellwidth
    @nextcellheight=newcellheight
    
  end 
   
  def createmap(robotname)
    #search the robot name
    @team.each_with_index { |member,index| 
    if(robotname==member.robotname)
      puts "building the map for #{robotname}, id #{index}"
      @mapno[@mapnum]=Robotmap.new(member)
      if(@nextmapwidth!=nil)
        @mapno[@mapnum].configbeliefmap( @nextmapwidth,@nextmapheight,@nextcellwidth,@nextcellheight) #Createbeliefma
      end
      @mapno[@mapnum].createbeliefmap()
      @mapno[@mapnum].autologmap(true)
      @mapno[@mapnum].autoupdatemap(true)
      @mapnum=@mapnum+1
      break
    end
    if(index==@num)
      puts "couldn't find robot with that name"
    end
    }
  end
  

end


#Test class
#testmap=Teammap.new()
#testmap.initmap
#testmap.putmap(1,0,0.3)
#testmap.putmap(-2,0,0.4)
#puts testmap.getmap(1,0)+testmap.getmap(-2,0)

