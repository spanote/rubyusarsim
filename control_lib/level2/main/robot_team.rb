#require path for unit tests
#require '../../level1/main/robot.rb'
#require '../../level1/support/robotlog.rb'

require 'control_lib/level1/main/robot.rb'
require 'control_lib/level1/support/robotlog.rb'

#used when using idm
require 'control_lib/level1/support/idm.rb'



class RobotTeam
    
  #global variables 
  attr_reader :robotno
  attr_reader :logno
  attr_reader :startpos

    
  def initialize(teamname,ip,port)
    @teamname=teamname
    @robotip=ip
    @robotport=port
    
    #robot number
    @num=0
    @robotno=[]
    
    #log number
    @lognum=0
    @logno=[]
    
    #startpos
    @startpos=[]
    
    #brain number (corrisponds to the robot number (uses @num))
    @brainno=[]

    #OPENGL
    @window = ""
  end
  
  #Config tasks
  def changeteamname(newname)
    if newname ==""
      puts "No new name entered "
    elsif newname.size>=20
      puts "Name too long "
    elsif newname =~/^\w+$/
      @teamname=newname
      puts "name changed to #{@teamname}"
    else
      puts "Please enter Alpha numeric characters only "
    end
    
  end
  
  
  def addlog(robotname)
    #search the robot name
    @robotno.each_with_index { |member,index| 
    if(robotname==member.robotname)
      
      puts "starting the log for #{robotname}, id #{index}"
      @logno[@lognum]=RobotLog.new(member,true)
      @logno[@lognum].reset_logs
      @logno[@lognum].startlog
      @lognum=@lognum+1
      
      break
    end
    if(index==@num)
      puts "couldn't find robot with that name"
    end
    }
    
  end
  
  
  def addrobot(robottype,start,name)

    #add a new robot class to the robot team
    @robotno[@num]=Robot.new(@robotip,@robotport)  
   
    #initialize the robot
    puts"Initializing robot number#{ @num} (#{name})"
    @robotno[@num].init(robottype,start,name)
    puts"Initializtion completed"
    
    #start recieving 
    @robotno[@num].recievestart
    
    #log the starting position
    startloc=[]
    startloc[0]=@robotno[@num].ins[0]
    startloc[1]=@robotno[@num].ins[1]
    @startpos[@startpos.length]=startloc
    
    
    #start idm library
    puts "starting the idm lib for #{name}"
    @brainno[@num]= Idm.new(@robotno[@num])
    puts "idmlib initiation completed"
    
    puts "robot number#{ @num} (#{name}) has been added"
    @num=@num+1
    
  end
  
  def runidm(robotname,idm)
    #search the robot name
    @robotno.each_with_index { |member,index| 
    if(robotname==member.robotname)      
      
      if(idm =~ /smartmove/i )   
        puts "starting the idm for #{robotname}, id #{index} to: #{idm}"
        @brainno[index].smartmoveloop #smart move is the smart randommove
      elsif(idm=~/mazemove/i )
        puts "starting the idm for #{robotname}, id #{index} to: #{idm}"
        @brainno[index].mazemove2loop
      else
        puts "cannot find idm function"
      end
      break
      
    end
    if(index==@num)
      puts "couldn't find robot with that name"
    end
    }
  end
  

end
