module Sensordata_module
   # if the camera is connected, sets the screen to mount the camera
 # assuming the robot's camera name at USARbot.ini is  Camera, FOV 0 is the default field of vision
 def mountcam(clientip)
   command="SET {Type Camera} {Robot " <<@robotname<<"} {Name Camera} {Client " <<clientip<< "}\r\n"
   puts command
   @connect.write(command)
  # sleep(1.0)
 end
 
 #dismount the camera
 def unmountcam(clientip)
 command="SET {Type Camera} {Client " <<clientip<< "}\r\n"
   puts command
   @connect.write(command)
   #sleep(1.0)
 end
 
 #gets the fsonarsensor from the sensor array and returns it as an array
 def getfsonarsensor(array)
   sen=array
   fson=[]
    sen.each{ |arraymember|
     #sonarsensors
      if arraymember[1].slice(5..arraymember[0].length-1)=="Sonar"
       arraymember.each{|sensinfo|
     if sensinfo[4..5]==" F"
          fson<<sensinfo[14..19]
         end
         }
      end
          
    }
   return fson
 end

 #gets the fsonarsensor from the sensor array and returns it as an array
 def getrsonarsensor(array)
   sen=array
   rson=[]
    sen.each{ |arraymember|
     #sonarsensors
      if arraymember[1].slice(5..arraymember[0].length-1)=="Sonar"
       arraymember.each{|sensinfo|
    if sensinfo[4..5]==" R"
          rson<<sensinfo[14..19]
         end
         }
      end
          
    }
   return rson
 end
 
 def getrangescanner(array)
    sen=array
    rangescanner=[]
    sen.each{|arraymember|
    if arraymember[1]=~/RangeScanner/i #search for RangeScanner Type
       arraymember.each{ |sensinfo|
        if sensinfo=~/Range/i #search for the Range element
          temparray=sensinfo.split(" ") #split into [Range][Value,Value,...]
          rangescanner=temparray[1].split(",")    
        end
       }
    end 
    }
    return rangescanner
 end
 

 #gets the INS infomation from the sensor array and returns it as an array
 def getinsinfo(array)
 sen=array
 insdata=[]
    sen.each{ |arraymember|
      #find INS sensor
      if arraymember[0].slice(5..arraymember[0].length-1)=="INS"
      insdata=[]
      ta1=arraymember[2].split(" ")
      ta1=ta1[1].split(",")
      insdata<<ta1[0]
      insdata<<ta1[1]
      insdata<<ta1[2]
      
      #Log the orientation sensor
      ta1=arraymember[3].split(" ")
      ta1=ta1[1].split(",")
      insdata<<ta1[0]
      insdata<<ta1[1]
      insdata<<ta1[2]
      end
    }
    return insdata
 end
end