#summary このページはRubyUSARsimのプログラミングライブラリーを利用した作成したロボット制御プログラムの例が記録している

===レベル１（ロボット制御レベル）を利用した作成したロボット制御プログラム===

ロボット制御レベルのコアモジュールは controllib/level1/main/robot.rb) に保存している。
ロボットレベルの制御プログラムを作成すると、必ずこのファイ
ルをインクルードしてください。

このクラスの新しいインスタンスを作成すれば、そのインスタンスのメソッドを呼び出すことによって(例：robot=Robot.new(ipstring,port))、新しいロボットオブジェットを作成します。コンストラクタ変数はUSARsimサーバIPアドレスとポート番号です。

{{{
#requires the robot team module
require 'control_lib/level1/main/robot.rb'

robot=Robot.new("10.1.157.59",3000)
}}}

ロボットオブジェットが作成されると、そのオブジェットを使ってロボットに命令を出せる。
しかし、その前に、ロボットをUSARsim世界に生成しなければならない。

{{{
robot.init("USARBot.P2AT","-6.0,-2.0,1.0","robot1")
}}}

次は、サーバからのロボットの情報を受けなければならない（センサーからの情報など）　robot.recievestartを利用する。

ロボットを先進するため、smartforのメソッドを利用する。そのメソッドの変数は　
距離と速度を使う。距離 は0.3 から10.0 の実数と速度 を1.0-3.0 の実数
を入力してください。たとえば、

{{{
 robot.smartfor(0.5,2.0) 
}}}

はロボットを0.5メートルに先進して、速度は2.0rad/secに先進する、 robot.smartback のメソッドは robot.smartforと同様だが、ロボットを後退する。

ロボットを回転するため、 robot.smartleft　と　robot.smartrightのメソッドを利用する。
メソッドが利用する変数は回転する角度（Radian）です。たとえば、

{{{
robot.smartleft(6.28/8) 
}}}

はロボットを左に45度を回転する。

init メソッドが利用する変数はロボットの種類、X,Y,Z 位置のスタート場所、ロボットの名前があります。現在のRubyUSARsimのライブラリーはP2ATというロボットしかサポートできないため、
ロボット種類は　"USARBot.P2AT"に設定しなければならない。


次は完全ランダムに移動するロボットの制御プログラムの例を示す。

{{{

#requires the robot team module
require 'control_lib/level1/main/robot.rb'

####### PROGRAM STARTS HERE ########################

#start the robot
robot=Robot.new("10.1.157.59",3000)
robot.init("USARBot.P2AT","-6.0,-2.0,1.0","robot1")
robot.recievestart
sleep(2)

#Main Loop
oldtime=Time.now
totaltime=0.0
checkno=20

while(totaltime<=60.0*10.0)
  puts "totaltime:#{totaltime}"
  choice=rand(10)
  robot.smartfor(0.5,2.0) if (choice<8)
  robot.smartback(0.5,2.0) if (choice==8)
  robot.smartleft(6.28/8) if (choice==9)
  robot.smartright(6.28/8)if (choice==10)
end #while

sleep(20)
}}}

*地図を利用する*

ロボットがOccupancy Gridの地図を生成するため、Robotmap クラスを利用する。　
このクラスは地図の生成クラスは　/control lib/level1/support/robotmap.rb のファイルに記録している。

ロボットマップのクラスを宣言するため、

{{{
map=Robotmap.new(robot)
}}}

を利用する、robot の変数はrobotクラスのオブジェットである。生成した map オブジェットは
robotmapのオブジェットになります。

オブジェットを生成した後に　autoupdatemap(true)　のメソッドを利用して、自動的にOccupancy Grid の地図の状態を更新する。　更新する方法はベイズと標準分散法を利用ている。
地図をメモリからテキストファイルに連続的に書き出すため、autologmap(true)　を利用する。

{{{
map.autoupdatemap(true)
map.autologmap(true)
}}}

Occupancy Grid の地図のログのファイルは　mapdata/occ (ロボットの名前) data.dat に生成される。

*ログファイルを自動的に生成する*

ログファイルを自動的に生成するため、RobotLog クラスを利用する。RobotLogのオブジェットを
生成するため、

{{{
robotlog=RobotLog.new(robot,true)　
}}}

を利用する。

ログを利用するため、まず古いログファイルを初期化する。reset_logs　のメソッドを利用する。
ロボットがログを自動的に生成するため、startlogのメソッドを利用する。

{{{
robotlog.reset_logs
robotlog.startlog
}}}


次は完全ランダムに移動するロボットの制御プログラムをログと地図生成機能を追加する

{{{

#requires the robot team module
require 'control_lib/level1/main/robot.rb'

####### PROGRAM STARTS HERE ########################

#start the robot
robot=Robot.new("10.1.157.59",3000)
robot.init("USARBot.P2AT","-6.0,-2.0,1.0","robot1")
robot.recievestart
sleep(2)

#start the robot logging
robotlog=RobotLog.new(robot,true)
robotlog.reset_logs
robotlog.startlog

#start the map
map=Robotmap.new(robot)
map.autoupdatemap(true)
map.autologmap(true)

#Main Loop
oldtime=Time.now
totaltime=0.0
checkno=20

while(totaltime<=60.0*10.0)
  puts "totaltime:#{totaltime}"
  choice=rand(10)
  robot.smartfor(0.5,2.0) if (choice<8)
  robot.smartback(0.5,2.0) if (choice==8)
  robot.smartleft(6.28/8) if (choice==9)
  robot.smartright(6.28/8)if (choice==10)
end #while

sleep(20)
}}}
